<?php

main();

function main() 
{
    $input = parseInputData();
    $coordinates = $input['coords'];
    foreach ($input['folds'] as $f) {
        $coordinates = fold($coordinates, $f);
    }

    printCoords($coordinates);
}

function fold($coordinates, $fold)
{
    $axis = $fold[0] === 'x' ? 0 : 1;
    $line = (int)$fold[1];
    $nextCoordinates = [];
    foreach($coordinates as $c) {
        if ($c[$axis] > $line) {
            $c[$axis] =  $line - ($c[$axis] - $line);
        }
        $nextCoordinates["{$c[0]}_{$c[1]}"] = $c;
    }
    return $nextCoordinates;
}

function printCoords($coordinates)
{
    $max = array_reduce($coordinates, fn ($v, $c) => [$v[0] < $c[0] ? $c[0] : $v[0], $v[1] < $c[1] ? $c[1] : $v[1]], [0,0]);
    for ($y = 0; $y <= $max[1]; $y++) {
        for ($x = 0; $x <= $max[0]; $x++) {
            if (isset($coordinates["{$x}_{$y}"])) {
                echo '#';
            } else {
                echo ' ';
            }
        }
        echo "\n";
    }
}

function parseInputData(): array
{
    $sections = explode("\n\n", file_get_contents(__DIR__ . '/13.input'));
    $points = explode("\n", $sections[0]);
    $coordinates = array_map(fn ($p) => array_map('intval', explode(',', $p)), $points);

    $folds = array_map(fn ($s) => explode('=', str_replace('fold along ', '', $s)), explode("\n", $sections[1]));
    return [
        'coords' => $coordinates,
        'folds' => $folds,
    ];
}