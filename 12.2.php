<?php

main();

function main()
{
	$input = parseInputData();
	// Get a graph of all nodes and where you can go from them
	$graph = buildGraph($input);
	// Get a list of all the small caves
	$smallCaves = getSmallCaves($graph);
	// Loop through each of the small caves and allow that cave to be used twice
	$paths = [];
	foreach ($smallCaves as $s) {
		// Build the paths for this setup.
		$paths = array_merge($paths, buildPaths($graph, $s));
	}
	// Map paths to strings.
	$pathStrings = array_map(fn ($p) => implode(',', $p), $paths);
	// Get array where length = number of unique strings (the values this produces are not used).
	$pathCounts = array_count_values($pathStrings);
	// Dump the number of paths.
	var_dump(count($pathCounts));die;
}

function getSmallCaves($graph)
{
	$smallCaves = [];
	foreach ($graph as $node => $values) {
		if ($node !== 'start' && strtolower($node) === $node) {
			$smallCaves[] = $node;
		}
	}
	return $smallCaves;
}

function buildGraph($input): array {
	$graph = [];
	foreach ($input as $i) {
		if ($i[1] !== 'start') {
			$graph[$i[0]]['adj'] = array_merge(($graph[$i[0]]['adj'] ?? []), [$i[1] => $i[1]]);
		}
		if ($i[0] !== 'start') {
			$graph[$i[1]]['adj'] = array_merge(($graph[$i[1]]['adj'] ?? []), [$i[0] => $i[0]]);
		}
	}
	unset($graph['end']);
	return $graph;
}

function buildPaths($graph, $allowedDouble, $paths = [['start']]): array
{
	$recurse = false;
	$newPaths = [];
	foreach ($paths as $p) {
		$currentPosition = end($p);

		if ($currentPosition === 'end') {
			$newPaths[] = $p;
			continue;
		}

		foreach ($graph[$currentPosition]['adj'] as $i) {
			// Do not use the same small cave twice except one cave
			$isLower = strtolower($i) === $i;
			if ($isLower && in_array($i, $p)) {
				if ($i !== $allowedDouble) {
					continue;
				}
				$counts = array_count_values($p);
				if (isset($counts[$allowedDouble]) && $counts[$allowedDouble] === 2) {
					continue;
				}
			}

			// Append the new position to the existing path.
			$recurse = true;
			$newPaths[] = array_merge($p, [$i]);
		}
	}

	if ($recurse) {
		return buildPaths($graph, $allowedDouble, $newPaths);
	}

	return $newPaths;
}

function parseInputData(): array
{
	return array_map(fn ($line) => explode('-', $line), explode("\n", file_get_contents(__DIR__ . '/12.input')));}