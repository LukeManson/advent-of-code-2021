<?php

main();

function main()
{
	$input = parseInputData();

	for ($i = 0; $i < 1000; $i++) {
		if (step($input) === 100) {
			var_dump($i + 1); die;
		}
	}
}

function step(&$input): int
{
	// Increase all by 1
	$input = array_map(fn ($line) => array_map(fn ($o) => $o + 1, $line), $input);

	$flashed = [];
	// Flash any higher than 9
	foreach ($input as $y => $line) {
		foreach ($line as $x => $o) {
			if ($o > 9) {
				flash($y, $x, $input, $flashed);
			}
		}
	}

	// Reset anything above 9 back to 0
	$input = array_map(fn ($line) => array_map(fn ($o) => $o > 9 ? 0 : $o, $line), $input);

	return count($flashed);
}

function flash($y, $x, &$input, &$flashed): void
{
	if (isset($flashed["{$y}_{$x}"])) {
		return;
	}

	$flashed["{$y}_{$x}"] = true;

	foreach (range(-1,1) as $dy) {
		foreach (range(-1, 1) as $dx) {
			if (isset($input[$y + $dy][$x + $dx])) {
				$input[$y + $dy][$x + $dx]++;
				if ($input[$y + $dy][$x + $dx] > 9) {
					flash($y + $dy, $x + $dx, $input, $flashed);
				}
			}
		}
	}
}

function parseInputData(): array
{
	return array_map(fn ($line) =>
	array_map(fn ($digit) => (int)$digit, str_split($line)),
		explode("\n", file_get_contents(__DIR__ . '/11.input'))
	);
}