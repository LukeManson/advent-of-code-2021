<?php

main();

function main() 
{
    $input = parseInputData();
    $coordinates = $input['coords'];
    $coordinates = $input['coords'];
    foreach ($input['folds'] as $f) {
        $coordinates = fold($coordinates, $f);
        var_dump(count($coordinates)); die;
    }
}

function fold($coordinates, $fold)
{
    $axis = $fold[0] === 'x' ? 0 : 1;
    $line = (int)$fold[1];
    $nextCoordinates = [];
    foreach($coordinates as $c) {
        if ($c[$axis] > $line) {
            $c[$axis] =  $line - ($c[$axis] - $line);
        }
        $nextCoordinates["{$c[0]}_{$c[1]}"] = $c;
    }
    return $nextCoordinates;
}

function parseInputData(): array
{
    $sections = explode("\n\n", file_get_contents(__DIR__ . '/13.input'));
    $points = explode("\n", $sections[0]);
    $coordinates = array_map(fn ($p) => array_map('intval', explode(',', $p)), $points);

    $folds = array_map(fn ($s) => explode('=', str_replace('fold along ', '', $s)), explode("\n", $sections[1]));
    return [
        'coords' => $coordinates,
        'folds' => $folds,
    ];
}