<?php

const MAP = [
	'(' => ')',
	'[' => ']',
	'{' => '}',
	'<' => '>',
];

const SCORE = [
	')' => 3,
	']' => 57,
	'}' => 1197,
	'>' => 25137,
];

main();

function main() 
{
    $input = parseInputData();

	$unexpected = [];
	foreach ($input as $line) {
		if ($u = checkSyntax($line)) {
			$unexpected[] = $u;
		}
	}

    $output = calculate($unexpected);
    var_dump($output);
}

function checkSyntax($line): ?string
{
	$expected = [];
	foreach ($line as $c) {
		if (isset(MAP[$c])) {
			$expected[] = MAP[$c];
			continue;
		}
		$e = end($expected);
		if ($e === $c) {
			array_pop($expected);
			continue;
		}

		return $c;
	}
	return null;
}

function calculate($data): int
{
	return array_reduce($data, fn ($carry, $i) => $carry += SCORE[$i]);
}

function parseInputData(): array
{
    return array_map('str_split', explode("\n", file_get_contents(__DIR__ . '/10.input')));
}