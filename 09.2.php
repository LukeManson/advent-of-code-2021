<?php

main();

function main()
{
	$input = parseInputData();

	$lows = findLows($input);

	$basins = measureBasins($lows, $input);

	$basinSizes = array_map(fn ($b) => count($b), $basins);
	uasort($basinSizes, fn ($a, $b) => $b - $a);

	$topThree = array_slice($basinSizes, 0, 3);
	$output = array_product($topThree);
	var_dump($output);die;
}

function measureBasins($lows, $input): array
{
	$basins = [];
	foreach ($lows as $l) {
		$basins[] = buildBasin($l['y'], $l['x'], $input, []);
	}

	return $basins;
}

function buildBasin($y, $x, $input, $basin)
{
	if (! isInBasin($y, $x, $input)) {
		return $basin;
	}
	$basin["{$y}_{$x}"] = $input[$y][$x];

	$adjacent = getNewAdjacentPoints($y, $x, $input, $basin);
	if (empty($adjacent)) {
		return $basin;
	}

	foreach ($adjacent as $a)
	{
		$basin = buildBasin($a['y'], $a['x'], $input, $basin);
	}

	return $basin;
}

function getNewAdjacentPoints($y, $x, $input, $basin): array
{
	$a = [
		[ 'y' => $y - 1, 'x' => $x ],
		[ 'y' => $y + 1, 'x' => $x ],
		[ 'y' => $y, 'x' => $x - 1 ],
		[ 'y' => $y, 'x' => $x + 1 ],
	];

	return array_filter($a, function($i) use ($input, $basin) {
		return isset($input[$i['y']][$i['x']]) && ! array_key_exists("{$i['y']}_{$i['x']}", $basin);
	});
}

function isInBasin($y, $x, $input): int
{
	if (! isset($input[$y][$x])) {
		return false;
	}

	if ($input[$y][$x] === 9) {
		return false;
	}

//	$lowerCount = 0;
//	// UP
//	if (isset($input[$y - 1][$x]) && $input[$y - 1][$x] < $input[$y][$x]) {
////		if ($y === 1 && $x === 8) {
////			var_dump($y - 1, $x, $input[$y - 1][$x]);die;
////		}
//		$lowerCount++;
//	}
//	// Down
//	if (isset($input[$y + 1][$x]) && $input[$y + 1][$x] < $input[$y][$x]) {
//		if ($y === 1 && $x === 8) {
//			var_dump('a', $y + 1, $x, $input[$y + 1][$x]);die;
//		}
//		$lowerCount++;
//	}
//	// Left
//	if (isset($input[$y][$x - 1]) && $input[$y][$x - 1] < $input[$y][$x]) {
//		if ($y === 1 && $x === 8) {
//			var_dump('b', $y, $x - 1, $input[$y][$x - 1]);die;
//		}
//		$lowerCount++;
//	}
//	// Right
//	if (isset($input[$y][$x + 1]) && $input[$y][$x + 1] < $input[$y][$x]) {
//		if ($y === 1 && $x === 8) {
//			var_dump('c', $y, $x + 1, $input[$y][$x + 1]);die;
//		}
//		$lowerCount++;
//	}
//	if ($lowerCount > 1) {
//		if ($y === 1 && $x === 8) {
//			var_dump("lower count $lowerCount");die;
//		}
//		return false;
//	}

	return true;
}

function findLows($input): array
{
	$lows = [];
	$yMax = count($input) - 1;
	foreach($input as $y => $line)
	{
		$xMax = count($line) - 1;
		foreach($line as $x => $digit)
		{
			// Check UP
			if ($y > 0 && $input[$y - 1][$x] <= $digit) {
				continue;
			}
			// Check Down
			if ($y < $yMax && $input[$y + 1][$x] <= $digit) {
				continue;
			}
			// Check Left
			if ($x > 0 && $line[$x - 1] <= $digit) {
				continue;
			}
			// Check Right
			if ($x < $xMax && $line[$x + 1] <= $digit) {
				continue;
			}
			$lows[] = ['x' => $x, 'y' => $y, 'z' => $digit];
		}
	}
	return $lows;
}

function calculate(array $lows): int
{
	$score = 0;
	foreach($lows as $l) {
		$score += $l['digit'] + 1;
	}
	return $score;
}

function parseInputData(): array
{
	return array_map(fn ($line) => array_map('intval', str_split($line)), explode("\n", $input = file_get_contents(__DIR__ . '/09.input')));
}