<?php

main();

function main()
{
	$input = parseInputData();

	$lows = [];
	$yMax = count($input) - 1;
	foreach($input as $y => $line)
	{
		$xMax = count($line) - 1;
		foreach($line as $x => $digit)
		{
			// Check UP
			if ($y > 0 && $input[$y - 1][$x] <= $digit) {
				continue;
			}
			// Check Down
			if ($y < $yMax && $input[$y + 1][$x] <= $digit) {
				continue;
			}
			// Check Left
			if ($x > 0 && $line[$x - 1] <= $digit) {
				continue;
			}
			// Check Right
			if ($x < $xMax && $line[$x + 1] <= $digit) {
				continue;
			}
			$lows[] = $digit;
		}
	}

	$output = calculate($lows);
	var_dump($output);
}

function calculate(array $lows): int
{
	$score = 0;
	foreach($lows as $l) {
		$score += $l + 1;
	}
	return $score;
}

function parseInputData(): array
{
	return array_map(fn ($line) => array_map('intval', str_split($line)), explode("\n", $input = file_get_contents(__DIR__ . '/09.input')));
}