<?php

main();

function main() 
{
    $fish = parseInputData();
    $fish = compressData($fish);
    $days = 256;
    for ($day = 0; $day < $days; $day++) {
        $fish = simulateDay($fish);
    }

    $output = calculate($fish);
    var_dump($output);
}

function compressData($fish)
{
    $output = [];
    foreach($fish as $f) {
        if (! isset($output[$f])) {
            $output[$f] = 0;
        }
        $output[$f]++;
    }
    return $output;
}

function simulateDay($fish)
{
    $nextDayOfFish = [];
    foreach($fish as $d => $c) {
        if ($d === 0) {
            if (! isset($nextDayOfFish[6])) {
                $nextDayOfFish[6] = 0;
            }
            $nextDayOfFish[6] += $c;

            if (! isset($nextDayOfFish[8])) {
                $nextDayOfFish[8] = 0;
            }
            $nextDayOfFish[8] += $c;
        } else {
            if (! isset($nextDayOfFish[$d - 1])) {
                $nextDayOfFish[$d - 1] = 0;
            }
            $nextDayOfFish[$d - 1] += $c;
        }
    }

    return $nextDayOfFish;
}

function calculate($fish) 
{
    return array_sum($fish);
}

function parseInputData() 
{
    $input = array_map('intval', explode(",", "1,1,3,5,3,1,1,4,1,1,5,2,4,3,1,1,3,1,1,5,5,1,3,2,5,4,1,1,5,1,4,2,1,4,2,1,4,4,1,5,1,4,4,1,1,5,1,5,1,5,1,1,1,5,1,2,5,1,1,3,2,2,2,1,4,1,1,2,4,1,3,1,2,1,3,5,2,3,5,1,1,4,3,3,5,1,5,3,1,2,3,4,1,1,5,4,1,3,4,4,1,2,4,4,1,1,3,5,3,1,2,2,5,1,4,1,3,3,3,3,1,1,2,1,5,3,4,5,1,5,2,5,3,2,1,4,2,1,1,1,4,1,2,1,2,2,4,5,5,5,4,1,4,1,4,2,3,2,3,1,1,2,3,1,1,1,5,2,2,5,3,1,4,1,2,1,1,5,3,1,4,5,1,4,2,1,1,5,1,5,4,1,5,5,2,3,1,3,5,1,1,1,1,3,1,1,4,1,5,2,1,1,3,5,1,1,4,2,1,2,5,2,5,1,1,1,2,3,5,5,1,4,3,2,2,3,2,1,1,4,1,3,5,2,3,1,1,5,1,3,5,1,1,5,5,3,1,3,3,1,2,3,1,5,1,3,2,1,3,1,1,2,3,5,3,5,5,4,3,1,5,1,1,2,3,2,2,1,1,2,1,4,1,2,3,3,3,1,3,5"));

    return $input;
}