<?php

const MAP = [
	'(' => ')',
	'[' => ']',
	'{' => '}',
	'<' => '>',
];

const SCORE = [
	')' => 1,
	']' => 2,
	'}' => 3,
	'>' => 4,
];

main();

function main() 
{
    $input = parseInputData();

	$completion = [];
	foreach ($input as $line) {
		if ($u = checkSyntax($line)) {
			$completion[] = $u;
		}
	}

    $output = calculate($completion);
    var_dump($output);
}

function checkSyntax($line): ?array
{
	$expected = [];
	foreach ($line as $c) {
		if (isset(MAP[$c])) {
			$expected[] = MAP[$c];
			continue;
		}
		$e = end($expected);
		if ($e === $c) {
			array_pop($expected);
			continue;
		}

		return null;
	}

	return array_reverse($expected);
}

function calculate($data): int
{
	$lineScores = array_map(fn ($i) => array_reduce($i, fn ($carry, $j) => ($carry * 5) + SCORE[$j]), $data);
	sort($lineScores);
	return $lineScores[(int)floor(count($lineScores)/2)];
}

function parseInputData(): array
{
    return array_map('str_split', explode("\n", file_get_contents(__DIR__ . '/10.input')));
}