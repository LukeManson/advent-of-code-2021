<?php

main();

function main() 
{
    $input = parseInputData();
	$graph = buildGraph($input);
	$paths = buildPaths($graph);
	var_dump(count($paths));die;
}

function buildGraph($input): array {
	$graph = [];
	foreach ($input as $i) {
		$graph[$i[0]]['adj'] = array_merge(($graph[$i[0]]['adj'] ?? []), [$i[1] => $i[1]]);
		$graph[$i[1]]['adj'] = array_merge(($graph[$i[1]]['adj'] ?? []), [$i[0] => $i[0]]);
	}
	return $graph;
}

function buildPaths($graph, $paths = [['start']]): array
{
	$recurse = false;
	$newPaths = [];
	foreach ($paths as $p) {
		$currentPosition = end($p);

		if ($currentPosition === 'end') {
			$newPaths[] = $p;
			continue;
		}

		foreach ($graph[$currentPosition]['adj'] as $i) {
			// Do not use the same route twice in the same direction
			$skip = false;
			foreach ($p as $k => $pos) {
				if ($pos !== $i) {
					continue;
				}
				if (isset($p[$k - 1]) && $p[$k - 1] === $currentPosition) {
					$skip = true;
					break;
				}
			}
			if ($skip) {
				continue;
			}

			// Do not use the same small cave twice
			$isLower = strtolower($i) === $i;
			if ($isLower && in_array($i, $p)) {
				continue;
			}

			// Append the new position to the existing path.
			$recurse = true;
			$newPaths[] = array_merge($p, [$i]);
		}
	}

	if ($recurse) {
		return buildPaths($graph, $newPaths);
	}

	return $newPaths;
}

function parseInputData(): array
{
    return array_map(fn ($line) => explode('-', $line), explode("\n", file_get_contents(__DIR__ . '/12.input')));
}